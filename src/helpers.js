// Convert time to hours and minutes
export const calcTime = time => {

    const hours = Math.floor(time / 60);
    const mins = time % 60;

    return `${hours}h ${mins}m`;
}

// Convert a number to money formatting
export const convertMoney = money => {

    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    });

    return formatter.format(money);

}

export const formatDate = (releaseDate) => {

    const today = new Date(releaseDate);
    // const dateTimeFormat = new Intl.DateTimeFormat('en', { day: 'numeric', month: 'short', year: 'numeric' });
    // const [{ value: month }, { value: day }, { value: year }] = dateTimeFormat.formatToParts(date);

    // Getting required values
    const year = today.getFullYear()
    const month = today.getMonth() +1
    const day = today.getDate()

    // Creating a new Date (with the delta)
    return `${day}-${month}-${year}`;
};

export const getReleasedYear = (releaseDate) => {

    const date = new Date(releaseDate);
    return date.getUTCFullYear();

};