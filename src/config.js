// COnfiguration for TMDB

const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = '3d9b77226774c501eb7239b6d6f7178f';

const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/';
const SEARCH_BASE_URL = `${API_URL}search/movie?api_key=${API_KEY}&query=`;
const POPULAR_BASE_URL = `${API_URL}movie/popular?api_key=${API_KEY}`;

// Sizes: w300, w780, w1280, original
const BACKDROP_SIZE = 'w1280';

// Sizes: w92, w154, w185, w1342, w500, w780, original
const POSTER_SIZE = 'w220_and_h330_face';

export { API_URL, API_KEY, IMAGE_BASE_URL, SEARCH_BASE_URL, POPULAR_BASE_URL, BACKDROP_SIZE, POSTER_SIZE }