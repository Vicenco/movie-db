import React from 'react';
import { Router } from '@reach/router';

import Header from './components/Header';

import Home from './view/Home';
import Movie from './view/Movie';
import NotFound from './view/NotFound';

import {createGlobalStyle} from 'styled-components';

const GlobalStyle = createGlobalStyle`
	body {
		margin: 0;
		padding: 0;
		box-sizing: border-box;
	}
`;

const App = () => {

	return (

		<>
			<Header />
			<Router>
				<Home path="/" />
				<Movie path="/:movieId" />
				<NotFound default />
			</Router>

			<GlobalStyle />
		</>

	);

}

export default App;
