import React from 'react';
import {StyledLoadMoreBtn} from '../styles/StyledLoadMoreBtn';

const LoadMoreBtn = ({ callback, text }) => {
    return (
        <StyledLoadMoreBtn type="button" onClick={callback}>
            {text}
        </StyledLoadMoreBtn>
    );
};

export default LoadMoreBtn;