import React, { useState, useRef } from 'react';
import { StyledSearchBar, StyledSearchBarContent } from '../styles/StyledSearchBar';

const SearchBar = ({callback}) => {

    const [state, setState] = useState('');
    const timeOut = useRef(null);

    const filterDB_Library = event => {

        const { value } = event.target;

        clearTimeout(timeOut.current);
        setState(value);

        timeOut.current = setTimeout(() => {
            callback(value);
        }, 500);
    };

    return (
        <StyledSearchBar>
            <StyledSearchBarContent>
                <input
                    type="text"
                    placeholer="Search for a movie, tv show, person..."
                    onChange={filterDB_Library}
                    value={state} />
            </StyledSearchBarContent>
        </StyledSearchBar>
    );
};

export default SearchBar;