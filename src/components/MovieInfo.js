import React from 'react';
import PropTypes from 'prop-types';

import MovieThumb from './MovieThumb';
import NoImage from '../images/no_image.jpg';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../config';
import { calcTime, formatDate, getReleasedYear } from '../helpers';

import { StyledMovieInfo } from '../styles/StyledMovieInfo';

const MovieInfo = ({ movie }) => (
	<StyledMovieInfo backdrop={movie.backdrop_path}>
		<div className="movieinfo-content">
			<div className="movieinfo-thumb">
				<MovieThumb
					image={
						movie.poster_path
							? `${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}`
							: NoImage
					}
					clickable={false}
					alt="moviethumb"
				/>
			</div>
			<div className="movieinfo-text">
				<section className="header poster">
					<div className="title ott_true">
						<h2 className="37">{movie.title} <span className="tag release_date">({getReleasedYear(movie.release_date)})</span></h2>
						<div className="facts">
							<span className="certification">PG-13</span>
							<span className="release">{ formatDate(movie.release_date)} ({movie.original_language.toUpperCase()})</span>
							<span className="genres">
								{
									movie.genres.map((genre, idx) => (
										<a key={genre.id} href="/">{genre.name}{idx === movie.genres.length - 1 ? '' : ','} </a>
									))
								}
							</span>
							<span className="runtime"><strong>Running time:</strong> {calcTime(movie.runtime)}</span>
						</div>
					</div>
					<ul className="auto actions">
						<li className="chart">
							<div className="consensus details">
								<div className="outer_ring">
									<div className="user_score_chart" data-percent="68.0" data-track-color="#423d0f" data-bar-color="#d2d531">
										<div className="percent">
											<span className="icon">{movie.vote_average}</span>
										</div>
										<canvas height="60" width="60"></canvas></div>
								</div>
							</div>
							<div className="text">IMDB<br />Rating</div>
						</li>

						<li className="video">
							<a className="play_trailer" href="/"><span className="play"></span>Play Trailer</a>
						</li>

					</ul>

					<div className="header_info">
						<h3 className="tagline">{movie.tagline}</h3>
						<h3>Overview</h3>
						<div className="overview">
							<p>{movie.overview}</p>
						</div>

						<ol className="people no_image">
							{
								movie.directors.map(element => (
									<li key={element.credit_id} className="profile">
										<p><a href="/person/10943-francis-lawrence?language=en-US">{element.name}</a></p>
										<p className="character">Director</p>
									</li>
								))
							}
						</ol>
					</div>
				</section>
			</div>
		</div>
	</StyledMovieInfo>
);

MovieInfo.propTypes = {
	movie: PropTypes.object,
	directors: PropTypes.array,
}

export default MovieInfo;
