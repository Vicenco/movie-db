import React from 'react';
import { Link } from '@reach/router';
import {StyledHeader, StyledLogo} from '../styles/StyledHeader';

const Header = () => {
    return (
        <StyledHeader>
            <div className="header-content">
                <Link to="/">
                    <StyledLogo src="/assets/logo.svg" />
                </Link>
            </div>
        </StyledHeader>
    );
};

export default Header;