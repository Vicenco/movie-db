
import styled from 'styled-components';

const StyledHeader = styled.div`
    background-color: black;
    box-sizing: border-box;
    padding: 0 20px;

    .header-content {
        max-width: 1280px;
        min-height: 65px;
        box-sizing: border-box;

        @media screen and (max-width: 500px) {
            min-height: 0;
        }

    }

`;
const StyledLogo = styled.img`
    width: 90px;
    margin-top: 10px;

    @media screen and (max-width: 500px) {
        width: 150px;
        margin-top: 5px;
    }

`;

export {StyledHeader, StyledLogo}