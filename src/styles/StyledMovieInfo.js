import styled from "styled-components";
import { IMAGE_BASE_URL, BACKDROP_SIZE } from "../config";

export const StyledMovieInfo = styled.div`
  background: ${(props) => (props.backdrop ? `url('${IMAGE_BASE_URL}${BACKDROP_SIZE}${props.backdrop}')` : "#000")};
  background-size: cover !important;
  background-position: center !important;
  width: 100%;
  padding: 40px 20px;
  box-sizing: border-box;
  animation: animateMovieinfo 1s;

  .movieinfo-content {
    max-width: 1280px;
    min-height: 450px;
    margin: 0 auto;
    background: rgb(0, 0, 0, 0.7);
    border-radius: 20px;
    position: relative;
  }

  .movieinfo-thumb {
    width: 300px;
    float: left;

    @media screen and (max-width: 768px) {
      width: 100% !important;
    }

  }

  .movieinfo-text {
    font-family: Arial, Helvetica, sans-serif;
    padding: 40px 40px 0;
    color: #fff;
    overflow: hidden;
    display: flex;

    .title {
      width: 100%;
      margin-bottom: 24px;
      display: flex;
      flex-wrap: wrap;
      
      a {
        color: #ffffff;
      }

      h2 {
        width: 100%;
        margin-bottom: 1rem;
        padding: 0;
        font-size: 2.2rem;

        @media screen and (max-width: 768px) {
          margin-bottom: 1rem
        }

        .release_date {
          opacity: 0.8;
          font-weight: 400;
        }

      @media screen and (max-width: 1000px) {
        font-size: 32px !important;
      }

    }


    .facts{
      display: flex;

      + span {
        padding-left: 0;
      }

      span + span {
        padding-left: 20px;
        position: relative;
        top: 0;
        left: 0;
      }
      .certification {
        border: 1px solid rgba(100.00%, 100.00%, 100.00%, 0.60);
        color: rgba(100.00%, 100.00%, 100.00%, 0.60);
        display: inline-flex;
        white-space: nowrap;
        align-items: center;
        align-content: center;
        padding: 0.06em 4px 0.15em 4px !important;
        line-height: 1;
        border-radius: 2px;
        margin-right: 7px;
      }
    }

    }

    @media screen and (max-width: 768px) {
      padding: 40px;
    }
    .actions {
      margin-bottom: 20px;
      width: 100%;
      height: 38px;
      display: flex;
      align-items: center;
      justify-content: flex-start;

      li {
        margin-right: 20px;
        list-style: none;

        &:last-of-type {
            margin-right: 0;
        }

        a {
          color: white;
          display: inline-flex;
          align-items: center;
          justify-content: center;
          align-content: center;
        }

      }
      .video {

        a {
          color: #ffffff;
          border: none;
          background: transparent;
          width: auto;
          height: auto;
          font-weight: 600;
          will-change: opacity;
          transition: linear .1s;
          box-sizing: border-box;
          opacity: 1;

          &:hover {
            opacity: 0.5;
          }
          
          span {
            margin-right: 5px;
            font-size: 1.4em;
          }

        }

      }


      .chart {
        background: transparent;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        box-sizing: border-box;
        height: 68px;

        .consensus {
          width: 68px;
          height: 68px;
          display: inline-block;
          transition: transform .2s;
          transform: scale(1);

          .outer_ring {
            display: inline-block;
            width: 48px;
            height: 48px;
            border-radius: 50%;
            padding: 4px;
            background-color: #081c22;

            .user_score_chart {
              position: relative;
              display: inline-block;
              width: 100%;
              height: 100%;
              text-align: center;

              canvas {
                background-color: transparent;
                position: absolute;
                top: 0;
                left: 0;
              }

              .percent {
                width: 100%;
                height: 100%;
                z-index: 2;
                display: flex;
                align-items: center;
                justify-content: center;

                .icon {
                  color: #fff;
                }
              }

            }
          }
        }
      }
    }
    
    .header_info {
      width: 100%;

      h3 {
        margin-top: 10px;
      }

      .tagline {
        margin-bottom: 0;
        font-size: 1.1em;
        font-weight: 400;
        font-style: italic;
        opacity: 0.7;
        width: 100%;
        margin: 0 0 1rem 0;
      }

      .overview {
        padding-top: 1rem;
      }
      p:last-child {
          margin-bottom: 0;
      }

    }

  .people {
    list-style-type: none;
    list-style-position: inside;
    margin: 2rem 0 0;
    padding: 0;
    display: flex;
    justify-content: space-between;
    position: relative;
    top: 0;
    left: 0;

    li {
      height: auto;
      margin-bottom: 0;

      p {
        font-size: 1em;
        margin: 0;
        padding: 0;
        overflow: hidden;
        text-overflow: ellipsis;

        a {
          font-weight: bold;
          color: white;
          font-size: 1.1rem;
        }
      }

    }



  }



  }

  .rating-director {
    display: flex;
    justify-content: flex-start;
  }

  .score {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 35px;
    height: 35px;
    background: #fff;
    color: #000;
    font-weight: 800;
    border-radius: 25px;
    margin: 0px 0 0 0;
  }

  .director {
    margin: 0 0 0 40px;

    p {
      margin: 0;
    }
  }

  @media screen and (max-width: 768px) {
    min-height: 600px;
    height: auto;
  }

  @keyframes animateMovieinfo {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;
