import styled from 'styled-components';

export const StyledLoadMoreBtn = styled.button`
  background: #000;
  width: 25%;
  min-width: 200px;
  height: 70px;
  color: #fff;
  cursor: pointer;
  transition: all 0.3s;
  border-radius: 40px;
  font-family: 'Poppins', sans-serif;
  font-size: 1.1rem;
  max-width: 100%;
  display: block;
  margin: 20px auto;
  padding: 0 20px;
  outline: none;
  border: 0;

  :hover {
    opacity: 0.8;
  }
`;
