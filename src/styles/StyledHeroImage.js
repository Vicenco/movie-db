
import styled from 'styled-components';

const StyledHeroImage = styled.div`
        background: ${props => 
        `linear-gradient(
            to bottom, rgba(0,0,0,0) 
            40%, rgba(0,0,0,0) 
            46%, rgba(0,0,0,0.8) 
            100%), url('${props.image}'), #1c1c1c`};

        background-size: cover;
        background-position: center, center;
        width: 100%;
        height: 600px;
        position: relative;
        animation: animatHeroImage 1s;
    

    .heroImage-content {
        max-width: 1280px;
        min-height: 20px;
        margin: 0 auto;
        box-sizing: border-box;

        .heroImage-text {
            z-index: 100;
            max-width: 700px;
            position: absolute;
            bottom: 40px;
            margin-right: 20px;
            min-height: 100px;
            color: white;

            h1 {
                font-family: 'Poppins', sans-serif;
                color: white;
                font-size: 2rem;
                margin-bottom: 0.8rem;

                @media screen and (max-width: 720px){
                    line-height: 20px;
                    margin-bottom: 1.5rem;
                }

            }

            p {
                font-family: 'Poppins', sans-serif;
                color: white;
                font-size: 1rem;
            }

            @media screen and (max-width: 720px){
                max-width: 100%;
                padding: 0 2rem;
            }

        }

    }

    @keyframes animatHeroImage {

        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }

    }

`;

export {StyledHeroImage};